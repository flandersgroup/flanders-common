#!/bin/sh

case "$1" in
"success")
    echo '{"schemaVersion":1, "label":"Build","message":"Passed","color":"success"}'
    ;;
"failed")
    echo '{"schemaVersion":1, "label":"Build","message":"Failed","color":"critical"}'
    ;;
"canceled")
    echo '{"schemaVersion":1, "label":"Build","message":"Canceled","color":"inactive"}'
    ;;
esac