# Configure docker gitlab runner

To run the pipeline instead of using public runner we can use runner install in docker as a service. Here are the steps to instal lthe runner and to be able to run the pipeline on it, especillay how to be able to run docker compose on it.

## Install gitlab runner as a docker service

To install the runner in docker we follow the [gitlab documentation to install runner](https://docs.gitlab.com/runner/install/docker.html#option-2-use-docker-volumes-to-start-the-runner-container) and install the runner using docker volume.

The two command to execute are in order : 
- `docker volume create gitlab-runner-config`
- `docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest`

Once done the runner is visible in the list of docker containers

## Register the runner

Then we need to register the gitlab runner in out project or group, the process is describe in [gitlab documentation to register a docker runner](https://docs.gitlab.com/runner/register/index.html#docker). 
Just run `docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register`. Then follow the instructions to enter :
- GitLab instance URL : https://gitlab.com/
- gitlab token : \<to find on your project or group\>
- runner description 
- the tags of the runner : for this project the tag flanders-backend must be in the list 
- the executor : docker

## Enable docker in docker and docker compose on the runner

To use docker and docker compose in our pipeline we need to enable it in our runner. This is explained in [gitlab documentation to use docker in docker runner](https://docs.gitlab.com/runner/register/index.html#docker). We need to modify the configuration of the runner in the file config.toml, this file is found in /etc/gitlab-runner/config.toml in the gitlab runner container. In this configuration file we need enable priviledges for the runner and to add docker socket : 

```
[[runners]]
...
  [runners.docker]
    privileged = true
    ...
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
```
