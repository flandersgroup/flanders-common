#!/bin/sh

comparison(){
	echo $1 $2 | awk '{ print ($1 > $2) ? "true" : "false" }'
}

thirdLine=`echo | awk 'NR==3 {print;exit}' $1`

lineRate=$(echo $thirdLine | grep -o -e 'line-rate="\(1\|0\.[0-9]*\)"' | grep -o -e '\(1\|0\.[0-9]*\)')

lineRatePercent=$(awk "BEGIN {print $lineRate*100}")

if [ $(comparison $lineRatePercent 80) = "true" ]
then
    echo "{\"schemaVersion\":1, \"label\":\"Coverage\",\"message\":\"$lineRatePercent %\",\"color\":\"success\"}"
elif [ $(comparison $lineRatePercent 60) = "true" ]
then
    echo "{\"schemaVersion\":1, \"label\":\"Coverage\",\"message\":\"$lineRatePercent %\",\"color\":\"important\"}"
else
    echo "{\"schemaVersion\":1, \"label\":\"Coverage\",\"message\":\"$lineRatePercent %\",\"color\":\"critical\"}"
fi