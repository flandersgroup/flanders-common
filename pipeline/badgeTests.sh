#!/bin/sh

secondLine=`echo | awk 'NR==2 {print;exit}' $1`

failureNumber=$(echo $secondLine | grep -o -e 'failures="[0-9]*' | grep -o -e '[0-9]*')

if [ $failureNumber =  0 ]
then
    echo '{"schemaVersion":1, "label":"Tests","message":"Passed","color":"success"}'
else
    totalNumber=$(echo $secondLine | grep -o -e 'tests="[0-9]*' | grep -o -e '[0-9]*')
    echo "{\"schemaVersion\":1, \"label\":\"Tests\",\"message\":\"$failureNumber/$totalNumber failed\",\"color\":\"critical\"}"
fi