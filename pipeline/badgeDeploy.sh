#!/bin/sh

case "$1" in
"success")
    echo '{"schemaVersion":1, "label":"Deployment","message":"Passed","color":"success"}'
    ;;
"failed")
    echo '{"schemaVersion":1, "label":"Deployment","message":"Failed","color":"critical"}'
    ;;
"canceled")
    echo '{"schemaVersion":1, "label":"Deployment","message":"Canceled","color":"inactive"}'
    ;;
esac