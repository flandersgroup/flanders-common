# Deploy the application

The pipeline of the backend and frontend are configures to push docker image to heroku when a tag with the format <code>v?[0-9]+[.][0-9]+[.][0-9]+</code> is push (v1.2.3 for example). The docker image have the name web for backend and frontend.

Once the image push on master to release it on heroku it is only needed to connect to heroku with <code>heroku login</code> and then release the image with : 
- for backend <code>heroku container:release -a flanders-backend web</code>
- for frontend <code>heroku container:release -a flanders-frontend web</code>
