export enum Authorizations {
    CanAdministrateUsers = 'CanAdministrateUsers',
    CanSeeHisPlanning = 'CanSeeHisPlanning',
    CanSeeHisAssignedMissions = 'CanSeeHisAssignedMissions',
    CanSeeHisSubmittedMissions = 'CanSeeHisSubmittedMissions',
    CanSubmitMission = 'CanSubmitMission',
    CanSeeHisProfile = 'CanSeeHisProfile'
  }
