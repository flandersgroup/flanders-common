export class LoginResponse {
    token: string = ''
    expires: Date = new Date(NaN)
}